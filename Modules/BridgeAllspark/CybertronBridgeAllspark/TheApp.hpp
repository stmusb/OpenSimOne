#pragma once
#include "cybertron/node/WorkAppWithHotArea.hpp"
//#include "AgentClient.hpp"
#include "cybertron/node/AgentClient.hpp"
#include "Node/Common.pb.h"
#include "Node/Agent.pb.h"
#include "cybertron/network/Message.hpp"
#include <iostream>
#include <WinSock2.h>

//#include "DataStruct.h"
#include "cybertron/glm/vec3.hpp"
#include "Node/HotArea.pb.h"
#include "SimOneIOStruct.h"


/////////////////////config center test
#include "cybertron/core/ConfigureClient.hpp"

#pragma comment(lib,"ws2_32")

struct Bridge2SimOneStruct
{
	char ctrlCmd[32] = "BRIDGE2SIMONE";
	char controllerName[32] = "";
	SimOne_Data_Control vehControl;
	char endFlag[4] = "END";
};

struct Bridge2AllsparkStruct
{
	char headCmd[32] = "SimOneWorkBridgeAllspark";
	SimOne_Data_Gps vehData;
	char endFlag[4] = "END";
};

#define theApp GET_APP_INSTANCE(TheApp)
class TheApp : public cybertron::WorkAppWithHotArea
{
public:
	TheApp();
	~TheApp();
	void testThread();
	bool onLoadConfig() override;
	virtual bool onTaskStart() override;
	bool onConnectNodeTimerAfter() override;
	//virtual bool onTaskStartAfter();
	virtual bool onTaskRunning();
	virtual bool onTaskStop();
	virtual void onWorkNodeStop();
	virtual bool onStart();
	virtual void onTimer(
		int framestamp,
		int ms);
	virtual void onTimer(
		int framestamp,
		int ms,
		int msModNs);
	virtual void onPlay();
	virtual void onPause();
	virtual void onReset();
	virtual std::string onControlCommand(
		const std::string controlCommand);
	virtual void onEnd(); // onEnd will be called when FromAnyRequestTaskEnd received
	virtual void onEnd(Common::ETaskEndReason reason);  // onEnd will be called when FromAnyRequestTaskEnd received, this CB with reason

	virtual void onCaseStart();
	virtual void onCaseStop();

	// TCP
	void StartTCPMultiPointServer();
	void ServerRecvTCP();

protected:
	
	void runDaemon();

protected:
	void getVerion();
	int run();
	void setNodeID(char * NodeID);
private:
	bool mbRunning;
	std::shared_ptr<cybertron::ConfigureClient> mpConfigCenter;
	//only for test.
	HotArea::MainVehiclePrivateData mMainVehiclePrivateData;
	float mOdometer = 0;
	cybertron::vec3	mLastPos;
	SimOne_Data_Gps mGpsInfo;
	cybertron::CybertronSyncConn mDynClient;
	int mBridgeAllsparkListenPort;
	vector<SOCKET> mConnNodeSocket;
	SOCKET mTempSocket;
	std::string mWorkID = "";
	std::string mVehicleId = "";
};

