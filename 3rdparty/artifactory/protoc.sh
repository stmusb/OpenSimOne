#!/bin/bash
case $(uname -s) in
    Linux*)     PACKAGE_FILE="protoc_linux.3.12.3.zip";;
    Darwin*)    PACKAGE_FILE="protoc_mac.3.12.3.zip";;
    MINGW*)     PACKAGE_FILE="protoc_win.3.12.3.zip";;
	*)          read -p "[ERROR] Unsupported operation system."; exit;
esac

if [ `uname -p` == 'aarch64' ]; then
   PACKAGE_FILE="protoc_linux_aarch64.3.12.3.zip"
fi

PACKAGE_NAME="protoc"
PACKAGE_URL="http://10.66.12.30:8081/artifactory/world51-3p/protoc/${PACKAGE_FILE}"
